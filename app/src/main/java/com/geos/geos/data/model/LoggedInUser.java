package com.geos.geos.data.model;

import java.io.Serializable;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class LoggedInUser implements Serializable {

    public String userId;
    private String token;
    private String displayName;
    private String location;

    public LoggedInUser(String userId,String token, String displayName) {
        this.userId = userId;
        this.token = token;
        this.displayName = displayName;
    }

    public String getUserId() {
        return userId;
    }
    public String getToken() {
        return token;
    }

    public String getDisplayName() {
        return displayName;
    }
}
