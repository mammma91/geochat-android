package com.geos.geos.data.model;

import android.graphics.Bitmap;
import android.net.Uri;

import java.io.Serializable;

public class ChatData implements Serializable {

    public Polygon[] polygon;
    public String id;
    public String name;
    public String description;
    public String imageUrl;
    public String lastMessage;
    public String lastTime;
    public Bitmap image;
    public int unreadMessage =0;
    public Uri uri;

    public ChatData(){}

    public ChatData(Polygon[] polygon, String id, String name, String description, String imageUrl) {
        this.polygon = polygon;
        this.id = id;
        this.name = name;
        this.description = description;
        this.imageUrl = imageUrl;
    }
}

