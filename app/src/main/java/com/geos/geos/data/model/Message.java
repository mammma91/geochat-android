package com.geos.geos.data.model;

import android.graphics.Bitmap;

public class Message {

    public static final int TYPE_MESSAGE = 0;
    public static final int TYPE_LOG = 1;
    public static final int TYPE_ACTION = 2;
    public static final int TYPE_MY_MESSAGE = 3;

    private int mType;
    private String mMessage;
    private String mUsername;
    private String mIconUser;
    private String mTimeMessage;
    private Message() {}

    public int getType() {
        return mType;
    };

    public String getMessage() {
        return mMessage;
    };

    public String getUsername() {
        return mUsername;
    };

    public String getIconUser() {
        return mIconUser;
    };

    public String getTimeMessage() {
        return mTimeMessage;
    };

    public static class Builder {
        private final int mType;
        private String mUsername;
        private String mMessage;
        private String mIconUser;
        private String mTimeMessage;

        public Builder(int type) {
            mType = type;
        }

        public Builder username(String username) {
            mUsername = username;
            return this;
        }

        public Builder message(String message) {
            mMessage = message;
            return this;
        }

        public Builder icon(String icon) {
            mIconUser = icon;
            return this;
        }

        public Builder time(String time) {
            mTimeMessage = time;
            return this;
        }

        public Message build() {
            Message message = new Message();
            message.mType = mType;
            message.mUsername = mUsername;
            message.mMessage = mMessage;
            message.mIconUser = mIconUser;
            message.mTimeMessage = mTimeMessage;
            return message;
        }
    }

}
