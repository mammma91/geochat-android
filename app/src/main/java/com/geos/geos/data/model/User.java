package com.geos.geos.data.model;

import android.graphics.Bitmap;

import com.geos.geos.ImageHelper;
import com.geos.geos.R;

import java.util.concurrent.ExecutionException;

public class User {
    private String userId;
    private String userName;
    private String iconPath;
    private Bitmap image;

    public User(String userId, String userName, String iconPath) {
        this.userId = userId;
        this.userName = userName;
        this.iconPath = "https://chatapi.atlant-mega.com"+ iconPath;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIcon() {
        return iconPath;
    }

    public void setIcon(String iconPath) {
        this.iconPath = "https://chatapi.atlant-mega.com" + iconPath;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Bitmap getImage() {
        return image;
    }
}
