package com.geos.geos.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.geos.geos.Cache;
import com.geos.geos.ChatActivity;
import com.geos.geos.ImageHelper;
import com.geos.geos.R;
import com.geos.geos.data.model.Message;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.content.ContentValues.TAG;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {
    private List<Message> mMessages;
    Context context;
    public String username="";

    public MessageAdapter(Context context, List<Message> messages) {
        this.context = context;
        mMessages = messages;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = -1;
        switch (viewType) {
            case Message.TYPE_MESSAGE:
                layout = R.layout.item_message;
                break;
            case Message.TYPE_LOG:
                layout = R.layout.item_log;
                break;
            case Message.TYPE_ACTION:
                layout = R.layout.item_action;
                break;
            case Message.TYPE_MY_MESSAGE:
                layout = R.layout.item_my_message;
                break;
        }
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Message message = mMessages.get(position);
        viewHolder.setMessage(message.getMessage());
        viewHolder.setUsername(message.getUsername());
        viewHolder.setUserIcon(message.getIconUser());
        viewHolder.setMessageTime(message.getTimeMessage());
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mMessages.get(position).getType();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mUsernameView;
        private TextView mMessageView;
        private ImageView mImageView;
        private TextView mTimeView;
        private String name="1";

        public ViewHolder(View itemView) {
            super(itemView);

            mUsernameView = (TextView) itemView.findViewById(R.id.username);
            mMessageView = (TextView) itemView.findViewById(R.id.message);
            mImageView = (ImageView) itemView.findViewById(R.id.imageViewIconChat);
            mTimeView = (TextView) itemView.findViewById(R.id.timeMessage);

        }

        public void setUsername(String username) {
            if (null == mUsernameView) return;
            name = username;
            mUsernameView.setText(username);
        }

        public void setMessage(String message) {
            if (null == mMessageView) return;
            mMessageView.setText(message);
        }

        public void setUserIcon(String icon){
            if (null == mImageView) return;
            Cache cache = new Cache(context);
            String nameFile = icon;
            nameFile = nameFile.substring(nameFile.lastIndexOf('/')+1,nameFile.lastIndexOf('.'));
            Uri uri = cache.getUriByFileName(nameFile);
            Log.i(TAG,"Uri set: "+ uri.getPath());
            Log.i(TAG,"Uri set: "+ uri.isRelative());
            Bitmap bitmap = null;
            if(!new File(uri.getPath()).exists()) {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
//            if(username.equals(name)){
//                System.out.println("!!!!!!!!!!----MyTYPE");
//                mImageView.setVisibility(View.GONE);
//                return;
//            }
//            username = name;
            if(bitmap!=null) {
                mImageView.setImageBitmap(bitmap);
            }else{
                mImageView.setImageResource(R.drawable.notification_badge);
            }
            //mImageView.setImageResource(R.drawable.notification_badge);
        }

        public void setMessageTime(String time){
            if (null == mTimeView) return;
            java.util.Date t = new java.util.Date((long)Long.parseLong(time));
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            mTimeView.setText(format.format(t));
        }
    }




}
