package com.geos.geos.data.model;

public class Polygon{
    private String lat;
    private String lng;

    public Polygon(String lat, String lng){
        this.lat = lat;
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }
}
