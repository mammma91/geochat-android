package com.geos.geos.data.model;

import java.io.Serializable;

public class TestMessage implements Serializable {

    public String roomId = "31b30079-ceb9-48a6-9740-9f4231bd83cf";
    public String userId = "5ed11ec7a3e0000f2008ac15";
    public String messageText = "Yoyoyo";
}
