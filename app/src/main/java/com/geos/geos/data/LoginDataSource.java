package com.geos.geos.data;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.geos.geos.data.model.LoggedInUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {
    LoggedInUser user;
    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: handle loggedInUser authentication
            user =null;
            ProgressTask task = new ProgressTask();
            String result = task.execute("https://chatapi.atlant-mega.com/api/auth/login",username,password).get();
            Log.i("ConnectResult",result);
            if(user!=null)
            return new Result.Success<>(user);
            else
                throw new Exception("");
        } catch (Exception e) {
            Log.i("ERROR", "login");
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }

    private class ProgressTask extends AsyncTask<String, Void, String> {

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... args) {

            String content;
            try {
                BufferedReader reader = null;

                    URL url = new URL(args[0]);
                    HttpURLConnection c = (HttpURLConnection) url.openConnection();
                    c.setRequestMethod("POST");
                    c.setRequestProperty("Content-Type", "application/json");
                    c.setRequestProperty("Accept", "application/json");
                    c.setDoOutput(true);
                    c.setDoInput(true);

                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("email", args[1]);
                    jsonParam.put("password", args[2]);
                    Log.i("JSON", jsonParam.toString());
                    DataOutputStream os = new DataOutputStream(c.getOutputStream());
                    os.writeBytes(jsonParam.toString());
                    os.flush();
                    Log.i("STATUS", String.valueOf(c.getResponseCode()));
                    content = String.valueOf(c.getResponseCode());
                    Log.i("MSG", c.getResponseMessage());
                    os.close();

                    try (BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "utf-8"))) {
                        StringBuilder response = new StringBuilder();
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            response.append(line);
                        }
                        System.out.println(response.toString());
                        JSONObject answer = new JSONObject(response.toString());
                        System.out.println(answer.get("userId"));
                        System.out.println(answer.get("token"));
                        user = new LoggedInUser(answer.get("userId").toString(),answer.get("token").toString(), answer.get("nickname").toString());
                    } catch (Exception ex) {
                       throw ex;
                    }


                    c.disconnect();

            } catch (IOException | JSONException ex) {
                content = ex.getMessage();
            }

            return content;
        }

        @Override
        protected void onPostExecute(String content) {
        }


    }
}


