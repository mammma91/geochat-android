package com.geos.geos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geos.geos.data.model.ChatData;
import com.geos.geos.data.model.LoggedInUser;
import com.geos.geos.data.model.TestMessage;
import com.geos.geos.data.model.User;
import com.geos.geos.login.LoginActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.location.LocationRequest;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static android.content.ContentValues.TAG;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {

    public static LoggedInUser user;
    public static ArrayList<User> users = new ArrayList<>();
    public static final String PREFS_FILE = "Account";
    private static final int REQUEST_ACCESS_TYPE=1;
    private static final int REQUEST_CODE_PERMISSION_READ_CONTACTS =1;
    private static String CHANNEL_ID = "Chat channel";
    AppBarConfiguration appBarConfiguration;
    public static Socket socket;
    public static Boolean sendNotify = true;
    public static int unreadMessage =0;
    public BadgeDrawable badgeDrawable;
    public static ArrayList<ChatData> chatDataArrayList = new ArrayList<>();
    public ImageView ava;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.SplashTheme);
        super.onCreate(savedInstanceState);
        login("in");
        checkPermissions();
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        setTitle("");
        DrawerLayout drawer = findViewById(R.id.mainContainer);
        final ConstraintLayout content = findViewById(R.id.fragment_main);
        ava = findViewById(R.id.avaView);
        //Uri uriAva = new ImageHelper(context).checkImagefromUrl("https://leonardo.osnova.io/87a25ad6-e784-7fb6-c68f-5a2e44fab5aa");
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, myToolbar, R.string.navigation_drawer_open, R.string
                .navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float slideX = drawerView.getWidth() * slideOffset;
                content.setTranslationX(slideX);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView slideView = findViewById(R.id.nav_view);
        BottomNavigationView navView = findViewById(R.id.nav_bottom_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications).setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        slideView.setNavigationItemSelectedListener(this);
        if(user!=null && user.getDisplayName()!=null){
            TextView userName = slideView.getHeaderView(0).findViewById(R.id.slideUserName);
            userName.setText(user.getDisplayName());
        }


        int menuItemId = navView.getMenu().getItem(2).getItemId();
        badgeDrawable = navView.getOrCreateBadge(menuItemId);
        if(unreadMessage>0) {
            badgeDrawable.setVisible(true);
            badgeDrawable.setNumber(unreadMessage);
        }else{
            badgeDrawable.setVisible(false);
        }
        badgeDrawable.setBadgeGravity(BadgeDrawable.TOP_END);
//BadgeDrawable.TOP_START
//BadgeDrawable.BOTTOM_END
//BadgeDrawable.BOTTOM_START

//        badgeDrawable.isVisible = false   //hide badge
//        badgeDrawable.clearNumber()


//        if (savedInstanceState == null) {
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.nav_host_fragment, new MapsFragment())
//                    .commit();
//        }
//        Button btnExit = findViewById(R.id.exit);
//        btnExit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                login("out");
//            }
//        });

        createNotificationChannel();
        context = getApplication();
        createSocket();
//        if(new File(uriAva.getPath()).exists()) {
//            try {
//                ava.setImageBitmap(MediaStore.Images.Media.getBitmap(context.getContentResolver(), uriAva));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
    }

    private void createSocket(){
        SocketApp app = new SocketApp();
        socket = app.getSocket();
        JSONObject jsonParam = new JSONObject();
        try {

            //jsonParam.put("chatId", "31b30079-ceb9-48a6-9740-9f4231bd83cf");
            //Toast.makeText(this,  jsonParam.toString(), Toast.LENGTH_SHORT).show();
            //Log.i("JSON", jsonParam.toString());
            socket.on(Socket.EVENT_CONNECT,onConnect);
            socket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            socket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            socket.on("event://get-message",getMessageNotify);
            socket.connect();


            //socket.emit("event://fetch_chats-info","5f679ba3941bb83380064d17".trim()); //"0f3d38eb-8eee-4d26-a83f-e2cd90403748" //"5f679ba3941bb83380064d17"
            //Toast.makeText(this, "Sended ", Toast.LENGTH_SHORT).show();
            //socket.emit("event://send-message",new TestMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//I/ContentValues: {"roomId":"31b30079-ceb9-48a6-9740-9f4231bd83cf","msg":{"id":"7ad3e4bd-111e-4373-849d-4610db0d3c5a","userId":"5ed11ec7a3e0000f2008ac15","userName":"mammma91","userAvatar":"\/userImages\/avatar-5ed11ec7a3e0000f2008ac15-1600845598418.png","messageText":".","messageTimeStamp":1600863552703}}

    private Ack refreshChats = new Ack() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!------------------!!!!!!!!!!!!!!!!!!!!");
                    Log.i(TAG, args.toString());
                    Log.i(TAG, args[0].toString());

                }
            });
        }
    };

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "Chat", importance);
            Uri ringURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();
            channel.setSound(ringURI, att);
            channel.setDescription("notify chat");
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private Emitter.Listener refreshMapData = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Refresh Map Data");
                }
            });
        }
    };

    private Emitter.Listener getMessageNotify = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(sendNotify) {
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!------------------!!!!!!!!!!!!!!!!!!!! Notify!!!!");
                        Log.i(TAG, args[0].toString());
                        String userName = "";
                        String message = "";
                        String time = "";
                        String roomIdString = "";
                        int roomId = 0;
                        JSONObject jsonObject = (JSONObject) args[0];
                        try {
                            roomId = jsonObject.getString("roomId").hashCode();
                            roomIdString = jsonObject.getString("roomId");
                            Log.i(TAG, "HashChat" + roomId);
                            userName = jsonObject.getJSONObject("msg").getString("userName");
                            message = jsonObject.getJSONObject("msg").getString("messageText");
                            time = jsonObject.getJSONObject("msg").getString("messageTimeStamp");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        unreadMessage = 0;
                        for(ChatData c :MainActivity.chatDataArrayList){
                            if(c.id.equals(roomIdString)){
                                roomIdString = c.name;
                                upUnread(roomIdString);
                            }
                            if(c.unreadMessage>0) unreadMessage++;
                        }
                        Intent notificationIntent = new Intent(MainActivity.this, MainActivity.class);
                        PendingIntent contentIntent = PendingIntent.getActivity(MainActivity.this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                        Uri ringURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                        NotificationCompat.Builder builder =
                                new NotificationCompat.Builder(context, CHANNEL_ID)
                                        .setSmallIcon(R.drawable.ic_baseline_send_24)
                                        .setContentTitle(roomIdString)
                                        .setContentText(message)
                                        .setContentIntent(contentIntent).setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                                        .setAutoCancel(true).setSound(ringURI)
                                        .setPriority(NotificationCompat.PRIORITY_HIGH);

                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
                        notificationManager.notify(roomId, builder.build());

                        if(unreadMessage>0) {
                            badgeDrawable.setVisible(true);
                            badgeDrawable.setNumber(unreadMessage);
                        }else {
                            badgeDrawable.setVisible(false);
                        }
                    }
                }
            });
        }
    };

    public static void upUnread(String id){
        for(ChatData c : chatDataArrayList){
            if(c.id.equals(id)){
                c.unreadMessage++;
            }
        }
    }

    public static void clearUnread(String id){
        for(ChatData c : chatDataArrayList){
            if(c.id.equals(id)){
                c.unreadMessage = 0;
            }
        }
    }

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting");
                    Toast.makeText(context, "Error connecting", Toast.LENGTH_LONG).show();
                }
            });
        }
    };


    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Connected");
                    Toast.makeText(context, "Connected", Toast.LENGTH_LONG).show();
                }
            });
        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        socket.emit("disconnect");
        socket.disconnect();
        socket.off(Socket.EVENT_CONNECT, onConnect);
        socket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        socket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        socket.off("event://get-message",getMessageNotify);
        Log.i(TAG, "Destroy Main!!!!!!!!!!!!!");
        Log.i(TAG, "Destroy Main!!!!!!!!!!!!!");
    }

    @Override
    public void onResume() {
        super.onResume();
        //Log.i(TAG, "Resume");
        if(unreadMessage>0) {
            badgeDrawable.setVisible(true);
            badgeDrawable.setNumber(unreadMessage);
        }else{
            badgeDrawable.setVisible(false);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        //Log.i(TAG, "Stop");
    }


//    private void startLocationUpdates() {
//        fusedLocationClient.requestLocationUpdates(locationRequest,
//                locationCallback,
//                Looper.getMainLooper());
//    }

    protected void login(String status){
        if(status.equals("in")){
            SharedPreferences account = getSharedPreferences(PREFS_FILE, MODE_PRIVATE);
            String id = account.getString("id","null");
            String token = account.getString("token","null");
            String userName = account.getString("name","null");
            if(!id.equals("null")&&!token.equals("null")){
                user = new LoggedInUser(id,token,userName);
                return;
            }
        }
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("status", status);
        startActivityForResult(intent, REQUEST_ACCESS_TYPE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode==REQUEST_ACCESS_TYPE){
            if(resultCode==RESULT_OK){
                Toast.makeText(getApplicationContext(), "Вход выполнен", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getApplicationContext(), "Ошибка доступа", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
        else{
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.setting, menu);
//        return true;
//    }
//
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if(item.getItemId() == R.id.sign_out) {
//            Toast.makeText(this, "SignOut clicked", Toast.LENGTH_SHORT).show();
//        }
//        return true;
//    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void checkPermissions(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_PERMISSION_READ_CONTACTS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_READ_CONTACTS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                } else {
                    login("out");
                }
                return;
        }


    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()){
            case R.id.sign_out:
                Toast.makeText(getApplicationContext(), "Sign Out", Toast.LENGTH_SHORT).show();
                login("out");
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.mainContainer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}