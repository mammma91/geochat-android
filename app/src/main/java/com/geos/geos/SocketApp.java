package com.geos.geos;

import android.app.Application;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;

public class SocketApp {

    public static final String CHAT_SERVER_URL = "https://chatapi.atlant-mega.com";//"http://192.168.31.78:5000";//"https://chatapi.atlant-mega.com/";
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(CHAT_SERVER_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }
}
