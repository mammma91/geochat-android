package com.geos.geos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.geos.geos.data.MessageAdapter;
import com.geos.geos.data.model.Message;
import com.geos.geos.data.model.User;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static android.content.ContentValues.TAG;

public class ChatActivity extends AppCompatActivity {

    private String chatId = "CHANNEL_ID";
    private String chatName = "CHANNEL_NAME";
    private String roomName = "observable-room";
    private TextInputEditText editText;

    private RecyclerView mMessagesView;
    private List<Message> mMessages = new ArrayList<Message>();
    private RecyclerView.Adapter mAdapter;
    private boolean mTyping = false;
    private Handler mTypingHandler = new Handler();
    private int tempDay=0;
    private JSONObject jsonParam;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        MainActivity.sendNotify = false;
        chatId = getIntent().getStringExtra("chatId");
        chatName = getIntent().getStringExtra("chatName");
        editText = (TextInputEditText) findViewById(R.id.textInputEditText);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.chatToolbar);
        setSupportActionBar(myToolbar);
        setTitle(chatName);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAdapter = new MessageAdapter(this, mMessages);
        mMessagesView = (RecyclerView) findViewById(R.id.recyclerView);
        mMessagesView.setLayoutManager(new LinearLayoutManager(this));
        mMessagesView.setAdapter(mAdapter);


        jsonParam = new JSONObject();
        try {
            jsonParam.put("userId", MainActivity.user.getUserId());
            jsonParam.put("chatId", chatId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MainActivity.socket.emit("event://join-chat", jsonParam, new Ack() {
            @Override
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!------------------!!!!!!!!!!!!!!!!!!!!");
                        Log.i(TAG, args[0].toString());
//                        addLog("You Join Chat");
//                        addParticipantsLog(2);
                    }
                });
            }
        });

        MainActivity.socket.emit("event://init-fetch-messages", jsonParam, new Ack() {
            @Override
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!------------------!!!!!!!!!!!!!!!!!!!!");
                        Log.i(TAG, args[0].toString());
                        JSONObject data = (JSONObject) args[0];
                        try {
                            JSONArray array = data.getJSONArray("data");
                            int length = array.length();
                            for(int i=0;i<length;i++){
                                JSONObject jsonObject = array.getJSONObject(i);
                                String chID = jsonObject.getString("chatId");
                                if(chatId.equals(chID)){
                                    parseJsonMessage(jsonObject.getJSONObject("message"));
                                }
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, e.getMessage());

                        }
                    }
                });
            }
        });

        MainActivity.socket.on("event://get-message",getMessage);

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!------------------!!!!!!!!!!!!!!!!!!!!");
        Log.i(TAG,"Users: "+ MainActivity.users.size());

        MainActivity.clearUnread(chatId);
    }

    public int findUser(String id){
        for(int i=0;i<MainActivity.users.size();i++){
            if(MainActivity.users.get(i).getUserId().equals(id)){
                return i;
            }
        }
        return -1;
    }

    public void parseJsonMessage(JSONObject jsonObject) throws JSONException {
        String userName="";
        String userId="";
        String message = "";
        String time = "";
        String icon = "";
        userName = jsonObject.getString("userName");
        userId = jsonObject.getString("userId");
        message = jsonObject.getString("messageText");
        time = jsonObject.getString("messageTimeStamp");
        try {
            if(jsonObject.has("userAvatar"))
                icon = jsonObject.getString("userAvatar");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        java.util.Date t = new java.util.Date((long)Long.parseLong(time));
        if(tempDay!=t.getDay()){
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-YY");
            addLog(format.format(t));
            tempDay=t.getDay();
        }
        int id = findUser(userId);
        User user = null;
        if(id == -1){
            user = new User(userId,userName,icon);
            checkImage(user);
            MainActivity.users.add(user);
            Log.i(TAG,"Users Create: "+ userName);
        }else{
            user = MainActivity.users.get(id);
            if(!user.getUserName().equals(userName))user.setUserName(userName);
            if(!user.getIcon().equals("https://chatapi.atlant-mega.com"+icon)) {
                user.setIcon(icon);
                checkImage(user);
            }
            Log.i(TAG,"Users exist: "+ userName);
        }
        if(!userId.equals(MainActivity.user.userId)) {
            addMessage(user.getUserName(), message,user.getIcon(),time);
        }else{
            addMyMessage(user.getUserName(), message,user.getIcon(),time);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK != resultCode) {
            MainActivity.socket.emit("event://close-chat", jsonParam);
            finish();
            return;
        }
    }

    public void checkImage(User user){
        Cache cache = new Cache(this);
        String nameFile = user.getIcon();
        nameFile = nameFile.substring(nameFile.lastIndexOf('/')+1,nameFile.lastIndexOf('.'));
        Uri uri = cache.getUriByFileName(nameFile);
        Log.i(TAG,"Uri path: "+ uri.getPath());
        Log.i(TAG,"Uri path: "+ uri.isRelative());
        if(!new File(uri.getPath()).exists()) {
            Log.i(TAG,"Image add: "+ nameFile);
            try {
                Bitmap image = new ImageHelper(this).getImage(user.getIcon());
                if (image != null) {
                    user.setImage(image);
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else{
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                user.setImage(bitmap);
                Log.i(TAG,"Image exist: "+ nameFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        MainActivity.socket.emit("event://close-chat", jsonParam);
        onBackPressed();
        return true;
    }


    public void sendMessage(View view) {
        String message = editText.getText().toString();
        if (message.length() > 0) {
            editText.getText().clear();
            //addMessage(MainActivity.user.getDisplayName(), message);
            JSONObject jsonParam = new JSONObject();
            try {
                jsonParam.put("userId", MainActivity.user.getUserId());
                jsonParam.put("chatId", chatId);
                jsonParam.put("message", message);
                MainActivity.socket.emit("event://send-message", jsonParam);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }



    private void addLog(String message) {
        mMessages.add(new Message.Builder(Message.TYPE_LOG).message(message).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private void addParticipantsLog(int numUsers) {
        addLog(getResources().getQuantityString(R.plurals.message_participants, numUsers, numUsers));
    }

    private void addMessage(String username, String message, String icon, String time) {
        mMessages.add(new Message.Builder(Message.TYPE_MESSAGE).username(username).message(message).icon(icon).time(time).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private void addMyMessage(String username, String message, String icon, String time) {
        mMessages.add(new Message.Builder(Message.TYPE_MY_MESSAGE).username(username).message(message).icon(icon).time(time).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private void addTyping(String username) {
        mMessages.add(new Message.Builder(Message.TYPE_ACTION)
                .username(username).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private void removeTyping(String username) {
        for (int i = mMessages.size() - 1; i >= 0; i--) {
            Message message = mMessages.get(i);
            if (message.getType() == Message.TYPE_ACTION && message.getUsername().equals(username)) {
                mMessages.remove(i);
                mAdapter.notifyItemRemoved(i);
            }
        }
    }


    private void scrollToBottom() {
        mMessagesView.scrollToPosition(mAdapter.getItemCount() - 1);
    }


    private Emitter.Listener getMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!------------------!!!!!!!!!!!!!!!!!!!!__CHAT");
                    Log.i(TAG, args[0].toString());
                    JSONObject data = (JSONObject) args[0];
                    try {
                        parseJsonMessage(data.getJSONObject("msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        MainActivity.socket.off("event://get-message",getMessage);
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.sendNotify = false;
        Log.i(TAG, "Resume Chat");
    }

    @Override
    public void onStop() {
        super.onStop();
        MainActivity.sendNotify = true;
        Log.i(TAG, "Stop Chat");
    }
}
