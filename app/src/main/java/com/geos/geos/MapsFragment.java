package com.geos.geos;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.geos.geos.data.model.ChatData;
import com.geos.geos.data.model.Polygon;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static android.content.ContentValues.TAG;
import static com.google.android.gms.maps.model.BitmapDescriptorFactory.defaultMarker;

public class MapsFragment extends Fragment implements GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener, OnMapReadyCallback, GoogleMap.OnPolygonClickListener,  GoogleMap.OnInfoWindowClickListener {
    private Socket mSocket;
    AsyncTask<String, Integer, Bitmap> icon;
    Marker marker;
    private GoogleMap mMap;
    private boolean firstPosition = true;
    private Context context;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private Location location;

//    private OnMapReadyCallback callback = new OnMapReadyCallback() {
//
//        /**
//         * Manipulates the map once available.
//         * This callback is triggered when the map is ready to be used.
//         * This is where we can add markers or lines, add listeners or move the camera.
//         * In this case, we just add a marker near Sydney, Australia.
//         * If Google Play services is not installed on the device, the user will be prompted to
//         * install it inside the SupportMapFragment. This method will only be triggered once the
//         * user has installed Google Play services and returned to the app.
//         */
//        @Override
//        public void onMapReady(GoogleMap googleMap) {
//            boolean success = googleMap.setMapStyle(new MapStyleOptions(getResources()
//                    .getString(R.string.style_map)));
//            // TODO: Before enabling the My Location layer, you must request
//            // location permission from the user. This sample does not include
//            // a request for location permission.
//            if (ContextCompat.checkSelfPermission(container, Manifest.permission.ACCESS_FINE_LOCATION)
//                    == PackageManager.PERMISSION_GRANTED) {
//                if (googleMap != null) {
//                    googleMap.setMyLocationEnabled(true);
//                }
//            } else {
//                // Permission to access the location is missing. Show rationale and request permission
//                PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
//                        Manifest.permission.ACCESS_FINE_LOCATION, true);
//            }
//            googleMap.setMyLocationEnabled(true);
//            googleMap.setOnMyLocationButtonClickListener(this);
//            googleMap.setOnMyLocationClickListener(this);
//            if (!success) {
//                Log.e(TAG, "Style parsing failed.");
//            }
//            LatLng sydney = new LatLng(-34, 151);
//            googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//            googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//        }
//    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = getView() != null ? getView() : inflater.inflate(R.layout.fragment_maps, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        Toolbar myToolbar = (Toolbar) getActivity().findViewById(R.id.my_toolbar);
        ColorDrawable colorDrawable = new ColorDrawable(Color.alpha(0));
        myToolbar.setBackground(colorDrawable);

        MainActivity.socket.emit("event://refresh-map-data",refreshChats);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(context, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(context, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        boolean success = googleMap.setMapStyle(new MapStyleOptions(getResources()
                .getString(R.string.style_map)));
       if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (googleMap != null) {
                mMap = googleMap;
                mMap.setMyLocationEnabled(true);
                mMap.setOnMyLocationButtonClickListener(this);
                mMap.setOnPolygonClickListener(this);
                locationRequest = LocationRequest.create();
                locationRequest.setInterval(60000);
                locationRequest.setFastestInterval(30000);
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                if(mFusedLocationClient == null) {
                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
                    mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
                }

                mMap.setOnInfoWindowClickListener(this);
           }
        }

    }

    private boolean isShow = false;
    private LocationCallback locationCallback = new LocationCallback(){
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            location = locationResult.getLastLocation();
            if(location != null && mMap !=null) {
                //mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),location.getLongitude())));
                if(firstPosition) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));
                    firstPosition = false;
                }

                Log.i(TAG, "Latitude"+location.getLatitude()+" : "+"Longitude"+location.getLongitude());
            }
//            if(!isShow && mMap !=null && MainActivity.chatDataArrayList.size()>0) {
//                for (ChatData chatData : MainActivity.chatDataArrayList) {
//                    addChatToMap(chatData);
//                }
//                isShow = true;
//            }
            for (ChatData chatData : MainActivity.chatDataArrayList) {
                try {
                    addChatToMap(chatData);
                }catch (Exception ex){

                }
            }
        }
    };

    public void addChatToMap(ChatData chatData){
        ArrayList<LatLng> latLngsArr = new ArrayList<>();
        for(Polygon p : chatData.polygon){
            latLngsArr.add(new LatLng(Double.valueOf(p.getLat()),Double.valueOf(p.getLng())));
        }
        com.google.android.gms.maps.model.Polygon polygon1 = mMap.addPolygon(new PolygonOptions()
                .clickable(true)
                .add(latLngsArr.toArray(new LatLng[0])));
        polygon1.setTag(chatData.name);
        polygon1.setStrokeWidth(2);
        polygon1.setStrokeColor(0xff405538);
        polygon1.setFillColor(0x44405538);
        marker = mMap.addMarker(new MarkerOptions()
                .position(getMid(latLngsArr))
                .title(chatData.name).snippet(chatData.description)); //defaultMarker(BitmapDescriptorFactory.HUE_AZURE) //.icon(BitmapDescriptorFactory.fromBitmap(icon))
        if(chatData.image==null && chatData.uri!=null){
            try {
                chatData.image = MediaStore.Images.Media.getBitmap(context.getContentResolver(), chatData.uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(chatData.image!=null) {
            Bitmap ico = Bitmap.createScaledBitmap(chatData.image, 100, 100, true);
            Canvas canvas = new Canvas(ico);
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(Color.GREEN);
            paint.setStrokeWidth(5);
            paint.setStyle(Paint.Style.STROKE);
            Path path = new Path();
            path.moveTo(160.0f, 240.0f);
            path.lineTo(140.0f, 200.0f);
            path.addArc(new RectF(140, 180, 180, 220), -180, 180);
            path.lineTo(160.0f, 240.0f);
            canvas.drawPath(path, paint);
            marker.setIcon(BitmapDescriptorFactory.fromBitmap(ico));
        }else{
            marker.setIcon(defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        }
    }

//"polygon":[{"lat":46.974629,"lng":31.992655},{"lat":46.974687,"lng":31.992489},{"lat":46.975143,"lng":31.992806},{"lat":46.975084,"lng":31.992977},{"lat":46.974629,"lng":31.992655}],"_id":"5f5d1cdbe38f3643dccd0ab3","id":"31b30079-ceb9-48a6-9740-9f4231bd83cf","name":"Step","description":"Step computer academy","imageUrl":"\/chatIcons\/avatar-31b30079-ceb9-48a6-9740-9f4231bd83cf-1599937755747.png"
    private Ack refreshChats = new Ack() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!--------ALL-CHATs----------!!!!!!!!!!!!!!!!!!!!");
                    Log.i(TAG, args[0].toString());
                    JSONObject data = (JSONObject) args[0];
                    try {
                        JSONArray dataArr = data.getJSONArray("data");
                        //MainActivity.chatDataArrayList.clear();
                        for(int i=0;i<dataArr.length();i++){
                            try {
                                ChatData chatData = parseChat(dataArr.getJSONObject(i));
                                if(chatData!=null){
                                    chatData.uri = new ImageHelper(context).checkImage(chatData.imageUrl);
                                    MainActivity.chatDataArrayList.add(chatData);
                                    Log.i(TAG, "List chat: "+chatData.name);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };

    public ChatData parseChat(JSONObject jsonObject){
        String name="";
        String id="";
        String description = "";
        String imageUrl = "";
        ArrayList<Polygon> arrPolygons = new ArrayList<>();
        try {
            name = jsonObject.getString("name");
            id = jsonObject.getString("id");
            if(jsonObject.has("description"))
                description = jsonObject.getString("description");
            if(jsonObject.has("imageUrl")) {
                imageUrl = "https://chatapi.atlant-mega.com"+jsonObject.getString("imageUrl");
            }
            JSONArray polygons = jsonObject.getJSONArray("polygon");
            for(int i=0;i<polygons.length();i++){
                JSONObject polygon = polygons.getJSONObject(i);
                String lat = polygon.getString("lat");
                String lng = polygon.getString("lng");
                Polygon p = new Polygon(lat, lng);
                arrPolygons.add(p);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return new ChatData(arrPolygons.toArray(new Polygon[arrPolygons.size()]),id,name,description,imageUrl);
    }


    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(locationCallback);
    }

    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Destroy");
    }
    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "Stop");
    }


    @Override
    public void onResume() {
        super.onResume();
        isShow = false;
        Log.i(TAG, "onResume");
    }
    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    @Override
    public void onPolygonClick(com.google.android.gms.maps.model.Polygon polygon) {
        // Flip from solid stroke to dotted stroke pattern.
        marker.showInfoWindow();
        Toast.makeText(context, polygon.getTag().toString(), Toast.LENGTH_SHORT).show();
    }

    static LatLng getMid(ArrayList<LatLng> arr){
        double x = (arr.get(0).latitude + arr.get(2).latitude)/2;
        double y = (arr.get(0).longitude + arr.get(2).longitude)/2;

        return new LatLng(x,y);
    }

    public Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            Toast.makeText(context, "Error Ico", Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Toast.makeText(context, "Info window clicked", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra("chatName", marker.getTitle());//getListView().getItemAtPosition(position).toString());
        for(ChatData c :MainActivity.chatDataArrayList){
            if(c.name.equals(marker.getTitle())){
                intent.putExtra("chatId", c.id);
                startActivityForResult(intent, 1);
                break;
            }
        }
    }

    //
    private class DownloadFilesTask extends AsyncTask<String, Integer, Bitmap> {
        protected Bitmap doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (IOException e) {
                Toast.makeText(context, "Error Ico", Toast.LENGTH_SHORT).show();
                return null;
            }
        }
    }

}