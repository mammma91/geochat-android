package com.geos.geos.ui.notifications;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.geos.geos.ChatActivity;
import com.geos.geos.ImageHelper;
import com.geos.geos.MainActivity;
import com.geos.geos.R;
import com.geos.geos.data.model.ChatData;
import com.geos.geos.login.LoginActivity;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import io.socket.client.Ack;

import static android.content.ContentValues.TAG;

public class NotificationsFragment extends ListFragment {

    private NotificationsViewModel notificationsViewModel;
    private Context context;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_chats, container, false);
        //final TextView textView = root.findViewById(R.id.text_notifications);
        notificationsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
               // textView.setText(s);
            }
        });
//        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#0F9D58"));
//        if(colorDrawable!=null)
//         getActivity().getActionBar().setBackgroundDrawable(colorDrawable);

        Toolbar myToolbar = (Toolbar) getActivity().findViewById(R.id.my_toolbar);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#306A925F"));
        myToolbar.setBackground(colorDrawable);
        JSONObject jsonParam = new JSONObject();
        try {
            jsonParam.put("userId", MainActivity.user.getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MainActivity.socket.emit("event://fetch_chats-info",jsonParam,getInfoChats);
        Log.i(TAG, "onCreateView Chat");
        return root;
    }

    private Ack getInfoChats = new Ack() {
        @Override
        public void call(final Object... args) {

                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!--------ALL-Online----------!!!!!!!!!!!!!!!!!!!!");
                    Log.i(TAG, args[0].toString());
                    JSONObject data = (JSONObject) args[0];

                    try {
                        JSONArray dataArr = data.getJSONObject("output").getJSONArray("chatsInfo");
                        //MainActivity.chatDataArrayList.clear();
                        for(int i=0;i<dataArr.length();i++){
                            try {
                                String chatId = dataArr.getJSONObject(i).getString("chatId");
                                String lastMessage = dataArr.getJSONObject(i).getJSONObject("lastMessage").getString("messageText");
                                String lastTime = dataArr.getJSONObject(i).getJSONObject("lastMessage").getString("messageTimeStamp");
                                for(ChatData chatData:MainActivity.chatDataArrayList){
                                    if(chatData.id.equals(chatId)){
                                        chatData.lastMessage = lastMessage;
                                        chatData.lastTime = lastTime;
                                        names.add(chatData.name);
                                        Log.i(TAG, chatData.name);
                                        chatDataMyList.add(chatData);
                                        break;
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


        }
    };
    ArrayList<ChatData> chatDataMyList = new ArrayList<>();
    ArrayList<String> names = new ArrayList<>();
    final String[] catNames = new String[]{"Рыжик", "Барсик", "Мурзик",
            "Мурка", "Васька", "Томасина", "Кристина", "Пушок", "Дымка",
            "Кузя", "Китти", "Масяня", "Симба"};


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_list_item_1, data);
//        setListAdapter(adapter);

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        MyListAdapter myListAdapter = new MyListAdapter(getActivity(), R.layout.listchatfragment_row,chatDataMyList.toArray(new ChatData[chatDataMyList.size()]));
        setListAdapter(myListAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public class MyListAdapter extends ArrayAdapter<ChatData> {

        private Context mContext;

        public MyListAdapter(Context context, int textViewResourceId,
                             ChatData[] objects) {
            super(context, textViewResourceId, objects);
            mContext = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // return super.getView(position, convertView, parent);

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.listchatfragment_row, parent, false);
            TextView nameTextView = (TextView) row.findViewById(R.id.textViewName);
            nameTextView.setText(chatDataMyList.get(position).name);
            TextView messageText = (TextView) row.findViewById(R.id.textViewMessage);
            messageText.setText(chatDataMyList.get(position).lastMessage);
            ImageView iconImageView = (ImageView) row.findViewById(R.id.imageViewIcon);
            TextView textViewTime = (TextView) row.findViewById(R.id.textViewTime);
            textViewTime.setText(getMessageTime(chatDataMyList.get(position).lastTime));

            // Присваиваем значок
//            Bitmap roundIcon = BitmapFactory.decodeResource(getResources(), R.drawable.cat);
//            //roundIcon = Bitmap.createScaledBitmap(roundIcon, 58, 58, true);
//            if(roundIcon!=null) {
//                roundIcon = ImageHelper.getRoundedCornerBitmap(roundIcon, 0);
//                iconImageView.setImageBitmap(roundIcon);
//            }else{
//                iconImageView.setImageResource(R.drawable.notification_badge);
//            }
            if(chatDataMyList.get(position).image==null)
                iconImageView.setImageResource(R.drawable.notification_badge);
            else
                iconImageView.setImageBitmap(chatDataMyList.get(position).image);
            return row;
        }
    }

    public String getMessageTime(String time){
        java.util.Date t = new java.util.Date((long)Long.parseLong(time));
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        return format.format(t);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        //Toast.makeText(getActivity(), getListView().getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra("chatName", chatDataMyList.get(position).name);
        intent.putExtra("chatId", chatDataMyList.get(position).id);//getListView().getItemAtPosition(position).toString());
        startActivityForResult(intent, 1);

    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Destroy Chat");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
    }
}