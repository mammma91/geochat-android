package com.geos.geos;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.geos.geos.data.model.User;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import static android.content.ContentValues.TAG;

public class ImageHelper   {
    Context context=null;
    ImageHelper(Context context){
        this.context = context;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        System.out.println("Width:"+width+" : Height-"+height);
        if(width>=height) width = height;
        else height = width;
        int leftTopX = bitmap.getWidth()/2-width/2;
        int leftTopY = bitmap.getHeight()/2-height/2;
        Bitmap output = Bitmap.createBitmap(width, width, Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, width, width);
        final Rect rect2 = new Rect(leftTopX*2, leftTopY*2, width, width);
        System.out.println("Width:"+width+" : X-"+leftTopX+" : Y-"+leftTopY);
        final RectF rectF = new RectF(rect);
        final float roundPx = width/2;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect2, rect, paint);

        return output;
    }

    public Bitmap getImage(String path) throws ExecutionException, InterruptedException {
        //Bitmap icon = new ImageHelper.DownloadFilesTask().execute(path).get();
        new ImageHelper.DownloadFilesTask().execute(path);
        return null;
    }


    private class DownloadFilesTask extends AsyncTask<String, Integer, Bitmap> {
        protected Bitmap doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                System.out.println(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                myBitmap = ImageHelper.getRoundedCornerBitmap(myBitmap, 0);
                System.out.println("Pre Save!!!!!!!!!!!!!!!!!");
                String nameFile = urls[0];
                nameFile = nameFile.substring(nameFile.lastIndexOf('/')+1,nameFile.lastIndexOf('.'));
                new Cache(context).saveToCacheAndGetUri(myBitmap,nameFile);
                return myBitmap;
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Load Icon Error!!!!!!!!!!!!!!!!!");
                return null;
            }
        }
    }

    public Uri checkImage(String path){
        Bitmap image=null;
        Cache cache = new Cache(context);
        String nameFile = path;
        nameFile = nameFile.substring(nameFile.lastIndexOf('/')+1,nameFile.lastIndexOf('.'));
        Uri uri = cache.getUriByFileName(nameFile);
        Log.i(TAG,"Uri path: "+ uri.getPath());
        if(!new File(uri.getPath()).exists()) {
            Log.i(TAG,"Image add: "+ nameFile);
            try {
                image = getImage(path);
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else{
            try {
                image = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                Log.i(TAG,"Image exist: "+ nameFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return uri;
    }

    public Uri checkImagefromUrl(String path){
        Bitmap image=null;
        Cache cache = new Cache(context);
        String nameFile = path;
        nameFile = nameFile.substring(nameFile.lastIndexOf('/')+1);
        Uri uri = cache.getUriByFileName(nameFile);
        Log.i(TAG,"Uri path: "+ uri.getPath());
        if(!new File(uri.getPath()).exists()) {
            Log.i(TAG,"Image add: "+ nameFile);
            try {
                image = getImage(path);
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else{
            try {
                image = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                Log.i(TAG,"Image exist: "+ nameFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return uri;
    }
}